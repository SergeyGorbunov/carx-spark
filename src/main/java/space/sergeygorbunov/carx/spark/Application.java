package space.sergeygorbunov.carx.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import space.sergeygorbunov.carx.spark.comparator.PlayerMoneyReversedComparator;
import space.sergeygorbunov.carx.spark.entity.Activity;
import space.sergeygorbunov.carx.spark.entity.Player;
import space.sergeygorbunov.carx.spark.entity.Registration;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.datastax.spark.connector.japi.CassandraJavaUtil.javaFunctions;
import static com.datastax.spark.connector.japi.CassandraJavaUtil.mapRowTo;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 4/18/2020
 * Time: 1:00 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class Application {
    public static void main(String[] args) {

        // context
        SparkContext context = new SparkContext(new SparkConf(true)
                .setMaster("local")
                .setAppName("carx-spark")
                .set("spark.cassandra.connection.host", "localhost")
                .set("spark.cassandra.auth.username", "cassandra")
                .set("spark.cassandra.auth.password", "cassandra"));

        // showRichPlayersByCountry(context, 10);
        // showRegistrationPlayersByCountry(context, DateUtils.addDays(new Date(), -1), DateUtils.addDays(new Date(), +1));
        // showActivityPlayer(context, "7685ccf4-81e6-44b9-a09b-42b7d0541517", DateUtils.addDays(new Date(), -1), DateUtils.addDays(new Date(), +1));
    }

    /**
     * Show activity player
     *
     * @param context
     * @param player
     * @param from
     * @param to
     */
    private static void showActivityPlayer(SparkContext context, String player, Date from, Date to) {
        javaFunctions(context)
                .cassandraTable("carx", "activities", mapRowTo(Activity.class))
                .filter(activity -> activity.getPlayer().equals(player) && activity.getTime().after(from) && activity.getTime().before(to))
                .collect()
                .stream()
                .sorted(Comparator.comparing(Activity::getTime))
                .forEach(System.out::println);
    }

    /**
     * Show rich players by country
     *
     * @param context
     * @param size
     */
    private static void showRichPlayersByCountry(SparkContext context, int size) {
        getPlayerCountries(context)
                .stream()
                .collect(Collectors.toMap(
                        country -> country,
                        country -> javaFunctions(context)
                                .cassandraTable("carx", "players", mapRowTo(Player.class))
                                .filter(player -> player.getCountry().equals(country))
                                .takeOrdered(size, new PlayerMoneyReversedComparator())))
                .forEach((country, players) -> players.forEach(System.out::println));
    }

    /**
     * Show registrations players by country
     *
     * @param context
     * @param from
     * @param to
     */
    private static void showRegistrationPlayersByCountry(SparkContext context, Date from, Date to) {
        getPlayerCountries(context)
                .stream()
                .collect(Collectors.toMap(
                        country -> country,
                        country -> javaFunctions(context)
                                .cassandraTable("carx", "registrations", mapRowTo(Registration.class))
                                .filter(registration -> registration.getCountry().equals(country) && registration.getTime().after(from) && registration.getTime().before(to))
                                .count()))
                .forEach((country, registrations) -> System.out.println(country + ": " + registrations));
    }

    /**
     * Get player countries
     *
     * @param context
     * @return
     */
    private static List<String> getPlayerCountries(SparkContext context) {
        return javaFunctions(context)
                .cassandraTable("carx", "players", mapRowTo(Player.class))
                .map(Player::getCountry)
                .distinct()
                .collect();
    }
}
