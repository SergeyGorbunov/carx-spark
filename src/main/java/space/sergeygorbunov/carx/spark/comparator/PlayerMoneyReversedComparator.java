package space.sergeygorbunov.carx.spark.comparator;

import space.sergeygorbunov.carx.spark.entity.Player;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 4/18/2020
 * Time: 6:13 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class PlayerMoneyReversedComparator implements Comparator<Player>, Serializable {
    @Override
    public int compare(Player o1, Player o2) {
        return o2.getMoney() - o1.getMoney();
    }
}
