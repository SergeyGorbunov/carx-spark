package space.sergeygorbunov.carx.spark.comparator;

import space.sergeygorbunov.carx.spark.entity.Activity;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 4/18/2020
 * Time: 6:31 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class ActivityTimeComparator implements Comparator<Activity>, Serializable {

    @Override
    public int compare(Activity o1, Activity o2) {
        return (int) (o1.getTime().getTime() - o2.getTime().getTime());
    }
}
