package space.sergeygorbunov.carx.spark.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 4/19/2020
 * Time: 1:05 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public class Registration implements Serializable {

    private String player;
    private String country;
    private Date time;

    public Registration() {
    }

    public Registration(String player, String country) {
        this.player = player;
        this.country = country;
        this.time = new Date();
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Registration{" +
                "player='" + player + '\'' +
                ", country='" + country + '\'' +
                ", time=" + time +
                '}';
    }
}
