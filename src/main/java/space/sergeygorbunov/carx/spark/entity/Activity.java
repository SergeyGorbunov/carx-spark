package space.sergeygorbunov.carx.spark.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 4/16/2020
 * Time: 11:38 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public class Activity implements Serializable {

    private String player;
    private int activity;
    private Date time;

    public Activity() {
    }

    public Activity(String player, int activity, Date time) {
        this.player = player;
        this.activity = activity;
        this.time = time;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public int getActivity() {
        return activity;
    }

    public void setActivity(int activity) {
        this.activity = activity;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "player='" + player + '\'' +
                ", activity=" + activity +
                ", time=" + time +
                '}';
    }
}
