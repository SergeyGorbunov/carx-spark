package space.sergeygorbunov.carx.spark.entity;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 4/16/2020
 * Time: 12:14 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public class Player implements Serializable {

    private String UUID;
    private String country;
    private int money;

    public Player() {
    }

    public Player(String UUID, String country, int money) {
        this.UUID = UUID;
        this.country = country;
        this.money = money;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return "Player{" +
                "UUID='" + UUID + '\'' +
                ", country='" + country + '\'' +
                ", money=" + money +
                '}';
    }
}
